package fr.epita.iam.iamlog;

import fr.epita.iam.iamlog.impl.IamLoggerManagerLog4j2Internal;

public class IamLoggerManager {
	
	private IamLoggerManager() {}
	
	public static IamLogger getIamLogger(Class<?> clazz) {
		return IamLoggerManagerLog4j2Internal.getIamLogger(clazz);
	}
	
	public static IamLogger getIamLogger(String className) {
		return IamLoggerManagerLog4j2Internal.getIamLogger(className);
	}
}
