package fr.epita.iam.iamlog.impl;

import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.message.MessageFactory;

import fr.epita.iam.iamlog.IamLogger;

class IamLoggerLog4j2Impl extends Logger implements IamLogger {
	protected IamLoggerLog4j2Impl(LoggerContext context, String name, MessageFactory messageFactory) {
		super(context, name, messageFactory);
	}
}
