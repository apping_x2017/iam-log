package fr.epita.iam.iamlog.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.LoggerContext;
import fr.epita.iam.iamlog.IamLogger;

public class IamLoggerManagerLog4j2Internal {
	private IamLoggerManagerLog4j2Internal() {}
	
	public static IamLogger getIamLogger(Class<?> clazz) {
		LoggerContext context = (LoggerContext) LogManager.getContext(clazz.getClassLoader(), false);
		return new IamLoggerLog4j2Impl(context, clazz.getName(), LogManager.getLogger().getMessageFactory());
	}
	
	public static IamLogger getIamLogger(String className) {
		LoggerContext context = (LoggerContext) LogManager.getContext(IamLoggerManagerLog4j2Internal.class.getClassLoader(), false);
		return new IamLoggerLog4j2Impl(context, className, LogManager.getLogger().getMessageFactory());
	}
}
