package fr.epita.iam.iamlog;

public interface IamLogger {
	void trace(String msg);
	void info(String msg);
	void debug(String msg);
	void warn(String msg);
	void error(String msg);
}
